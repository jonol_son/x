package interpreter;

import interpreter.bytecodes.ByteCode;
import java.util.ArrayList;


 /**
  * Program.java
  * the program class is responsible for keeping the 'memory addresses of code 
  * and resolving the addresses for goto and label calls
  * @author Jonathan
  */

public class Program 
{
	private ArrayList <ByteCode> codes;
	private ArrayList <Integer> labelList;
	private ArrayList <Integer> gotoList;
	private int pc;

        /**
         * Default constructor
         */
	public Program() 
        {
		pc = 0;
		codes = new ArrayList();
		labelList = new ArrayList();
		gotoList = new ArrayList();
	}

        /**
         * returns the address 
         * @param pc
         * @return 
         */
        
	public ByteCode getCode(int pc) 
        {
		return codes.get(pc);
	}

        /**
         * adds code to codes and either label or goto list 
         * if neccesary and increases pc
         * @param bytecode 
         */
	public void putCode(ByteCode bytecode) 
        {
		String codeName = bytecode.getId();
		if (codeName.matches("LABEL"))
                {
			labelList.add(pc);
                }
		else if (codeName.matches("FALSEBRANCH|GOTO|CALL|RETURN"))
                {
			gotoList.add(pc);
                }
		codes.add(bytecode);
		pc++;
	}

        /** 
         * resolves addresses so that goto and label list point to correct 
         * 'memory location'
         */
	public void resolveAddresses() 
	{
                //for each goto get the label
		for (int gotoLine : gotoList) 
		{
			ByteCode code = codes.get(gotoLine);
			String gotolabel = code.getArgs();

                        //for each label get the label
			for (int i : labelList) 
			{
				String labelTemp = codes.get(i).getArgs();
                                //compare the two labels
				if (labelTemp.equals(gotolabel)) 
				{
					String arg = code.getArgs() + " " + Integer.toString(i);
					code.init(arg);
					codes.set(gotoLine, code);
					break;
				}
			}
		}
	}
}