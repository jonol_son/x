package interpreter;

import interpreter.bytecodes.ByteCode;
import java.util.Stack;

 /**
  * VirtualMachine.java
  * the virtual machine keeps the program counter and is responsible 
  * for 'running' the program (as well as printing dump)
  * @author Jonathan
  */

public class VirtualMachine 
{  
    public int pc;
    public RunTimeStack runStack;
    public Stack<Integer> returnAddrs;
    public Boolean isRunning;
    public Boolean isDump;
    public Program program;

    /**
     * loads the program with bytecodes in vm
     * @param programIn 
     */
    public VirtualMachine(Program programIn) {
        this.program = programIn;
    }

    /**
     * runs program executing the code by program counter
     */
    public void executeProgram() {
        pc = 0;
        runStack = new RunTimeStack();
        returnAddrs = new Stack();
        isRunning = true;
        //isDump = true; //for debug
        isDump = false;

        while (isRunning) {
            ByteCode code = program.getCode(pc);
            code.execute(this);
            if (isDump)
                dump(code);
            pc++;
        }
    }

    /**
     * dumps the bytecode information
     * @param code 
     */
    private void dump(ByteCode code) 
    {
        String codeName = code.getId();
        String builder = "";
        if (!codeName.matches("DUMP")) 
        {
            String[] argList = code.getArgs().split("\\s");
            builder += codeName+" "+argList[0]+" ";

                if (codeName.matches("LIT")) 
                {
                    if (argList.length > 1)
                        builder += argList[1];
                } 
                else if (codeName.matches("CALL")) 
                {
                    String funcName = argList[0].split("<<")[0];
                    String funcArgs = "";
                    for (int i = runStack.peekFrame(); i < runStack.runtimeStackSize(); i++) 
                    {
                        funcArgs += runStack.get(i);
                        if (i != runStack.runtimeStackSize()-1)
                            funcArgs += ",";
                    }
                    builder += "   "+funcName+"("+funcArgs+")";
                }
                else if (codeName.matches("LOAD")) 
                {
                    builder += argList[1]+ "   "+"<load "+argList[1]+">";
                } 
                else if (codeName.matches("STORE")) 
                {
                    builder += argList[1]+ "   ";
                } 
                else if (codeName.matches("RETURN")) 
                {
                    String funcName = argList[0].split("<<")[0];
                    builder += "   " +"exit "+funcName+": "+runStack.peek();
                } 
            
            // Print string and dump the stack
            System.out.println(builder+"\t");
            runStack.dump();
        }
    }
    /**
     * sets the virtual machines program counter
     * @param programCounter 
     */
    public void setProgramCounter(int programCounter) 
    {
        pc = programCounter;
    }
    
    /**
     * returns the current program counter
     * @return program counter
     */
    public int getProgramCounter() 
    {
        return pc;
    }

    /**
     * halts the virtual machine
     */
    public void halt() 
    {
        isRunning = false;
    }
    
    /**
     * returns the runtime stack size
     * @return size of runtimestack
     */
    public int runtimeStackSize() 
    {
        return runStack.runtimeStackSize();
    }
    
    /**
     * returns the top element of the runtimestack
     * @return top of runtime stack
     */
    public int peekRuntimeStack() 
    {
        return runStack.peek();
    }

    /**
     * pops and returns top item off runtimestack
     * @return top item from stack 
     */
    public int popRuntimeStack() 
    {
        return runStack.pop();
    }

    /**
     * pushes an int on runtime stack
     * @param value
     * @return item pushed on stack
     */
    public int pushRuntimeStack(int value) 
    {
        return runStack.push(value);
    }

    /**
     * pushes an integer on runtime stack
     * @param value
     * @return item pushed on stack
     */
    public Integer pushRuntimeStack(Integer value) 
    {
        return runStack.push(value);
    }

    /**
     * Pops the top item off the runtime stack and stores it at offset
     * @param offset
     * @return 
     */
    public int storeRuntimeStack(int offset) 
    {
        return runStack.store(offset);
    }

    /**
     * gets item at offset and pushes it at the top of the stack
     * @param offset
     * @return 
     */
    public int loadRuntimeStack(int offset) 
    {
        return runStack.load(offset);
    }

    /**
     * create a new frame at offset
     * @param offset 
     */
    public void newRuntimeStackFrameAt(int offset) 
    {
        runStack.newFrameAt(offset);
    }

    /**
     * removes the frame
     */
    public void popRuntimeStackFrame() 
    {
        runStack.popFrame();
    }

    /**
     * called from DumpCode to toggle dump
     * @param dump 
     */
    public void dumpRuntimeStack(Boolean dump) 
    {
        isDump = dump;
    }

    /**
     * pops the address off the return address stack
     * @return return address 
     */
    public int popReturnAddress() 
    {
        return returnAddrs.pop();
    }

    /**
     * pushes the address on return address stack
     * @param address
     * @return return address
     */
    public int pushReturnAddress(int address) 
    {
        return returnAddrs.push(address);
    }
    
}