package interpreter;

import java.util.ArrayList;
import java.util.Stack;

 /**
  * RuntimeStack.java
  * The runtime stack is responsible for maintaining the programs runtime stack
  * @author Jonathan
  */

public class RunTimeStack 
{
    private ArrayList<Integer> runStack;
    private Stack<Integer> framePointers;

    /**
     * creates new runtime stack with dummy on bottom
     */
    public RunTimeStack() 
    {
        runStack = new ArrayList<>();
        framePointers = new Stack<>();
        framePointers.add(0);
    }

    /**
     * Runtime Stack dump of contents
     */
    public void dump()
    {
        if(runStack.size()>0)
        {
            System.out.print("[");
            for (int i = 0; i < runStack.size(); i++) 
            {
                if (i != 0 && framePointers.contains(i))
                    System.out.print("] [");

                if (!framePointers.contains(i))
                    System.out.print(",");

                System.out.print(runStack.get(i));
            }        
            System.out.print("]");
        }
        System.out.println("");
    }

    /**
     * returns top item on runtime stack
     * @return top item on stack
     */
    public int peek() 
    {
        return runStack.get(runStack.size()-1);
    }

    /**
     * gets the item at specific index (ie not necessarily top)
     * @param index
     * @return returns item at index
     */
    public int get(int index) 
    {
        return runStack.get(index);
    }

    /**
     * pops the top item off the stack
     * @return 
     */
    public int pop() 
    {
        return runStack.remove(runStack.size()-1);
    }

    /**
     * creates a new frame at offset position
     * @param offset 
     */
    public void newFrameAt(int offset) 
    {
        framePointers.add(offset);
    }

    /**
     * removes frame and pushes top item on the stack
     */
    public void popFrame() 
    {
        int topItem = runStack.get(runStack.size()-1);
        
        int framePointer = framePointers.pop();
        int frameSize = runStack.size() - framePointer;
        for (int i = 0; i < frameSize; i++)
            runStack.remove(framePointer);

        runStack.add(topItem);
    }

    /**
     * returns the top frame item
     * @return 
     */
    public int peekFrame() 
    {
        return framePointers.peek();
    }

    /**
     * pushes value onto the stack
     * @param value
     * @return 
     */
    public int push(int value) 
    {
        runStack.add(value);
        return value;
    }

    /**
     * pushes value onto the stack
     * @param value
     * @return value pushed on stack
     */
    public Integer push(Integer value) 
    {
        runStack.add(value);
        return value;
    }

    /**
     * pops the top item off the stack and stores the offset
     * @param offset
     * @return 
     */
    public int store(int offset) 
    {
        int frameOffset = framePointers.peek() + offset;
        runStack.set(frameOffset, runStack.get(runStack.size()-1));
        return runStack.remove(runStack.size()-1);
    }

    /**
     * looks up item at frame+offset and pushes to top of stack
     * @param offset
     * @return 
     */
    public int load(int offset) 
    {
        int frameOffset = framePointers.peek() + offset;
        runStack.add(runStack.get(frameOffset));
        return runStack.get(runStack.size()-1);
    }

    /**
     * method to get runtimestack size
     * @return stack size
     */
    public int runtimeStackSize() 
    {
        return runStack.size();
    }

    /**
     * returns the frame size
     * @return size of frame
     */
    public int frameSize() 
    {
        return framePointers.size();
    }
}