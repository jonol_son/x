package interpreter;

import interpreter.bytecodes.ByteCode;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

 /**
  * ByteCodeLoader.java
  * Bytecodeloader is responsible for loading the bytecodes 
  * @author Jonathan
  */

public class ByteCodeLoader 
{

    private BufferedReader source;
    
    /**
     * sets the sourcereader to the codefile
     * @param codeFile
     * @throws IOException 
     */
        public ByteCodeLoader(String codeFile) throws IOException 
        {
        if (codeFile=="NULL") 
            throw new IOException("Need Filename");
      
        source = new BufferedReader(new FileReader(codeFile));
        }

    /**
     * loads the codes from the file
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException 
     */
    public Program loadCodes() throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException 
    {
        Program program = new Program();

            while (source.ready()) 
            {
                String line = source.readLine();

                String arr[] = line.split("\\s",2);
                String className = arr[0];
                //check if there are arguements
                String args;
                if (arr.length > 1)
                    args = arr[1];
                else
                    args = "";
                
                String codeClass = "interpreter.bytecodes." + getCodeClass(arr[0]);
                //String codeClass = "interpreter.bytecodes.ArgsCode";
                ByteCode bytecode = (ByteCode)(Class.forName(codeClass).newInstance());
                
                bytecode.init(args);                
                program.putCode(bytecode);
            }

        source.close();
        program.resolveAddresses();
        return program;
    }

    public String getCodeClass(String code) 
    {
        String codeClass = CodeTable.get(code);
        return codeClass;
    }
}