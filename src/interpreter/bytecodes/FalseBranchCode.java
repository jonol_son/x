package interpreter.bytecodes;

import interpreter.VirtualMachine;

 /**
  * FalseBranchCode.java 
  * @author Jonathan
  */

public class FalseBranchCode extends ByteCode
{
    private String label;
    
    public FalseBranchCode(){}

    //init constructor
    public void init(String args) 
    {
        label = args.trim();
    }

    //returns args
    public String getArgs() 
    {
        return label;
    }
    
    //execute method
    public void execute(VirtualMachine vm) 
    {
        if (vm.popRuntimeStack() == 0) 
        {
              int address = Integer.parseInt(label.split(" ")[1]);
              vm.setProgramCounter(address);
        }
    }
}