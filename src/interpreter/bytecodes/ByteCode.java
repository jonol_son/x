package interpreter.bytecodes;

import interpreter.VirtualMachine;

 /**
  * ByteCode.java 
  * @author Jonathan
  */

public abstract class ByteCode 
{
    //returns the bytecode id
    public String getId() 
    {
        String codeName = this.getClass().getName().replaceAll("interpreter.bytecodes.", "").replaceAll("Code", "");
        return codeName.toUpperCase();
    }
    
    //abstract methods to be implimented by specific bytecodes
    public abstract void init(String args);
    public abstract String getArgs();
    public abstract void execute(VirtualMachine aThis);
}