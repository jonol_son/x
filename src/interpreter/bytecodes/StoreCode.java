package interpreter.bytecodes;

import interpreter.VirtualMachine;

 /**
  * StoreCode.java 
  * @author Jonathan
  */

public class StoreCode extends ByteCode 
{
    private int offset;
    private String arg;
    
    public StoreCode(){}

    //init constructor
    public void init(String args) 
    {
        arg = args;
        String value = args.split(" ")[0];
        offset = Integer.parseInt(value);
    }

    //returns args
    public String getArgs() 
    {
        return arg;
    }
    
    //execute method
    public void execute(VirtualMachine vm) 
    {
        vm.storeRuntimeStack(offset);
    }
}