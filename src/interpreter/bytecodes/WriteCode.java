package interpreter.bytecodes;

import interpreter.VirtualMachine;

 /**
  * WriteCode.java 
  * @author Jonathan
  */

public class WriteCode extends ByteCode 
{
    public WriteCode(){}

    //init constructor
    public void init(String args){}

    //returns args
    public String getArgs() 
    {
        return "";
    }
    
    //execute method
    public void execute(VirtualMachine vm) 
    {
        System.out.println(vm.peekRuntimeStack());
    }
}