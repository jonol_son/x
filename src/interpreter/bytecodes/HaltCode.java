package interpreter.bytecodes;

import interpreter.VirtualMachine;

 /**
  * HaltCode.java 
  * @author Jonathan
  */

public class HaltCode extends ByteCode 
{
    public HaltCode(){}

    //init constructor
    public void init(String args) {}
    
    //returns args
    public String getArgs() 
    {
        return "";
    }
    
    //execute method
    public void execute(VirtualMachine vm) 
    {
        vm.halt();
    }
}