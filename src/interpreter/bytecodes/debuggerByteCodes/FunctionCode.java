package interpreter.bytecodes.debuggerByteCodes;

import interpreter.VirtualMachine;
import interpreter.bytecodes.ByteCode;
import interpreter.debugger.DebugVM;

/**
 * FunctionCode Debugger ByteCode
 * @author Jonathan
 */
public class FunctionCode extends ByteCode 
{
    private String args;

    public void init(String arg) 
    {
        args = arg;
    }

    /**
     * executes FunctionCode
     * @param vm 
     */
    public void execute(VirtualMachine vm) 
    {
        DebugVM curr = (DebugVM)vm;
        int beginLine = Integer.parseInt(args.split(" ")[1]);
        int endLine = Integer.parseInt(args.split(" ")[2]);
        curr.addFunctionRecord(args.split(" ")[0], beginLine, endLine);
        curr.setCurrentLine(beginLine);
    }
    
    /**
     * returns the functioncode's args
     * @return args
     */
    public String getArgs() 
    {
        int beginLine = Integer.parseInt(args.split(" ")[1]);
        int endLine = Integer.parseInt(args.split(" ")[2]);
        return args.split(" ")[0] + " " + beginLine + " " + endLine;
    }
}