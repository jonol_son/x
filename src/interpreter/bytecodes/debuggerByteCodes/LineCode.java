package interpreter.bytecodes.debuggerByteCodes;

import interpreter.VirtualMachine;
import interpreter.bytecodes.ByteCode;
import interpreter.debugger.DebugVM;

/**
 * LineCode ByteCode
 * @author Jonathan
 */
public class LineCode extends ByteCode 
{
    private int lineNumber;

    public void init(String args) 
    {
        lineNumber = Integer.parseInt(args);
    }

    /**
     * executes LineCode
     * @param vm 
     */ 
    public void execute(VirtualMachine vm) 
    {
        DebugVM curr = (DebugVM)vm;
        curr.setCurrentLine(lineNumber);
    }
    
    /**
     * gets bytecode args
     * @return bytecodeargs
     */
    public String getArgs() 
    {
        return "" + lineNumber;
    }

}