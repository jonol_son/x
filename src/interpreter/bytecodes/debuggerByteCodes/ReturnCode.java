package interpreter.bytecodes.debuggerByteCodes;

import interpreter.VirtualMachine;
import interpreter.debugger.DebugVM;

/**
 * ReturnCode Debugger ByteCode
 * @author Jonathan
 */
public class ReturnCode extends interpreter.bytecodes.ReturnCode 
{
/**
 * executes ReturnCode
 * @param vm 
 */
    public void execute(VirtualMachine vm) 
    {
        DebugVM curr = (DebugVM)vm;
        super.execute(curr);
        curr.popFunctionRecord();
    }
}