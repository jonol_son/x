package interpreter.bytecodes.debuggerByteCodes;

import interpreter.VirtualMachine;
import interpreter.debugger.DebugVM;

/**
 * LitCode Debugger ByteCode
 * @author Jonathan
 */
public class LitCode extends interpreter.bytecodes.LitCode 
{
    /**
     * executes LitCode
     * @param vm 
     */
    public void execute(VirtualMachine vm) 
    {
        DebugVM curr = (DebugVM)vm;
        super.execute(curr);
        
        if (!id.isEmpty()) 
        {
            int offset = curr.runtimeStackSize() - 1;
            curr.addRecordEntry(id, offset);
        }
    }
}