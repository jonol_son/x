package interpreter.bytecodes.debuggerByteCodes;

import interpreter.debugger.DebugVM;
import interpreter.VirtualMachine;

/**
 * PopCode Debugger ByteCode
 * @author Jonathan
 */
public class PopCode extends interpreter.bytecodes.PopCode 
{
    /**
     * executes PopCode
     * @param vm 
     */
    public void execute(VirtualMachine vm) 
    {
        DebugVM curr = (DebugVM)vm;
        super.execute(curr);
        curr.popRecordEntries(numOfPops);
    }
}