package interpreter.bytecodes.debuggerByteCodes;

import interpreter.bytecodes.ByteCode;
import interpreter.debugger.DebugVM;
import interpreter.VirtualMachine;

/**
 * FormalCode Debugger ByteCode
 * @author Jonathan
 */
public class FormalCode extends ByteCode 
{
    private String args;

    public void init(String arg) 
    {
        args = arg;
    }

     /**
     * executes FormalCode
     * @param vm 
     */
    public void execute(VirtualMachine vm) 
    {
        DebugVM curr = (DebugVM)vm;
        int offset = Integer.parseInt(args.split(" ")[1]);
        curr.addRecordEntry(args.split(" ")[0], offset + curr.runtimeStackSize() - 1);
    }
    
    /**
     * returns the arguments
     * @return arguments
     */
    public String getArgs() 
    {
        int offset = Integer.parseInt(args.split(" ")[1]);
        return args.split(" ")[0] + " " + offset;
    }

}