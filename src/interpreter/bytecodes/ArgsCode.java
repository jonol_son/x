package interpreter.bytecodes;

import interpreter.VirtualMachine;

 /**
  * ArgsCode.java 
  * @author Jonathan
  */

public class ArgsCode extends ByteCode 
{
    private String arg;
    public ArgsCode(){}

    //init constructor
    public void init(String args) 
    {
        arg = args;
    }
    
    //returns args
    public String getArgs() 
    {
        return arg;
    }
    
    //execute method
    public void execute(VirtualMachine vm) 
    {
        int frame = vm.runtimeStackSize() - Integer.parseInt(arg);
        vm.newRuntimeStackFrameAt(frame);
    }
}