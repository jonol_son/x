package interpreter.bytecodes;

import interpreter.VirtualMachine;

 /**
  * ReturnCode.java 
  * @author Jonathan
  */

public class ReturnCode extends ByteCode 
{
    private String fname;
    
    public ReturnCode(){}

    //init constructor
    public void init(String args) 
    {
        fname = args;
    }

    //returns args
    public String getArgs() 
    {
        return fname;
    }
    
    //execute method
    public void execute(VirtualMachine vm) 
    {
        vm.popRuntimeStackFrame();
        vm.setProgramCounter(vm.popReturnAddress());
    }
}