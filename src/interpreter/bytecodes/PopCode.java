package interpreter.bytecodes;

import interpreter.VirtualMachine;

 /**
  * PopCode.java 
  * @author Jonathan
  */

public class PopCode extends ByteCode 
{
    protected int numOfPops;
    public PopCode() {}
    

    //init constructor
    public void init(String args) 
    {
        numOfPops = Integer.parseInt(args);
    }

    //returns args
    public String getArgs() 
    {
        return Integer.toString(numOfPops);
    }
    
    //execute method
    public void execute(VirtualMachine vm) 
    {
         for (int i = 0; i < numOfPops; i++)
            vm.popRuntimeStack();
    }
}