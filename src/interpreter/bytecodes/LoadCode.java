package interpreter.bytecodes;

import interpreter.VirtualMachine;

 /**
  * LoadCode.java 
  * @author Jonathan
  */

public class LoadCode extends ByteCode 
{
    private int offset;
    private String arg, id;
    
    public LoadCode(){}

    //init constructor
    public void init(String args) 
    {
        arg = args;
        String arg = args.split(" ")[0];
        offset = Integer.parseInt(arg);
    }
    
    //returns args
    public String getArgs() 
    {
        return arg;
    }
    
    //execute method
    public void execute(VirtualMachine vm) 
    {
        vm.loadRuntimeStack(offset);
    }
}