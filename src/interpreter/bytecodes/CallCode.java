package interpreter.bytecodes;

import interpreter.VirtualMachine;

 /**
  * CallCode.java 
  * @author Jonathan
  */

public class CallCode extends ByteCode 
{
    private String fname;
    public CallCode(){}

    //init constructor
    public void init(String args) 
    {
        fname = args;
    }
    
    //returns args
    public String getArgs() 
    {
        return fname;
    }
    
    //execute method
    public void execute(VirtualMachine vm) 
    {
        vm.pushReturnAddress(vm.getProgramCounter());

        String fNumber = fname.split(" ")[1];
        int address = Integer.parseInt(fNumber);
        vm.setProgramCounter(address-1);
    }
}