package interpreter.bytecodes;

import interpreter.VirtualMachine;

 /**
  * DumpCode.java 
  * @author Jonathan
  */

public class DumpCode extends ByteCode 
{
    private Boolean dump;
    
    public DumpCode() {}

    //init constructor
    public void init(String args) 
    {
        if (args.trim().equals("ON"))
        {
            dump = true;
        }
        else
        {
            dump = false;
        }
    }

    //returns args
    public String getArgs() 
    {
        return "";
    }
    
    //execute method
    public void execute(VirtualMachine vm) 
    {
        vm.dumpRuntimeStack(dump);
    }
}