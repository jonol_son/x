package interpreter.bytecodes;

import interpreter.VirtualMachine;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

 /**
  * ReadCode.java 
  * @author Jonathan
  */

public class ReadCode extends ByteCode 
{
    public ReadCode(){}

    //init constructor
    public void init(String args){}
   
    //returns args
    public String getArgs() 
    {
        return "";
    }
    
    //execute method
    public void execute(VirtualMachine vm) 
    {  
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String input = "";
        System.out.print("Enter a number: ");
        try 
        {
            input = in.readLine();
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(ReadCode.class.getName()).log(Level.SEVERE, null, ex);
        }
        int number = Integer.parseInt(input);
        vm.pushRuntimeStack(number);
    }
}