package interpreter.bytecodes;

import interpreter.VirtualMachine;

 /**
  * BopCode.java 
  * @author Jonathan
  */

public class BopCode extends ByteCode
{
	private String op;
	private int solution;

	public BopCode(){}

	//init constructor
	public void init(String args)
	{
		op = args;
	}
        
	//returns args
	public String getArgs()
	{
		return op;
	}
        
	//execute method
	public void execute(VirtualMachine vm)
	{
		int a = vm.popRuntimeStack();
		int b = vm.popRuntimeStack();
		// addition
		if (op.equals("+"))
			solution = b + a;
		// subtraction
		else if (op.equals("-"))
			solution = b - a;
		// multiplication
		else if (op.equals("*"))
			solution = b * a;
		// division
		else if (op.equals("/"))
			solution = b / a;

		// equals
		else if (op.equals("=="))
		{
			if (b == a)
				solution = 1;
			else
				solution = 0;
		}
		// does not equals
		else if (op.equals("!="))
		{
			if (b != a)
				solution = 1;
			else
				solution = 0;
		}
		// less than equal to
		else if (op.equals("<="))
		{
			if (b <= a)
				solution = 1;
			else
				solution = 0;
		}
		// greater than equal to
		else if (op.equals(">="))
		{
			if (b >= a)
				solution = 1;
			else
				solution = 0;
		}
		// less than
		else if (op.equals("<"))
		{
			if (b < a)
				solution = 1;
			else
				solution = 0;
		}
		// greater than
		else if (op.equals(">"))
		{
			if (b > a)
				solution = 1;
			else
				solution = 0;
		}
		// and
		else if (op.equals("&"))
		{
			if ((b > 0) && (a > 0))
				solution = 1;
			else
				solution = 0;
		}
		// or
		else if (op.equals("|"))
		{
			if ((b > 0) || (a > 0))
				solution = 1;
			else
				solution = 0;
		}

		vm.pushRuntimeStack(solution);
	}
}