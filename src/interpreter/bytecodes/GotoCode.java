package interpreter.bytecodes;

import interpreter.VirtualMachine;

 /**
  * GotoCode.java 
  * @author Jonathan
  */

public class GotoCode extends ByteCode 
{
    private String label;
    
    public GotoCode(){}

    //init constructor
    public void init(String args) 
    {
        label = args;
    }

    //returns args
    public String getArgs() 
    {
        return label;
    }
    
    //execute method
    public void execute(VirtualMachine vm) 
    {
        String gotoNumber = label.split(" ")[1];
        int address = Integer.parseInt(gotoNumber);
        vm.setProgramCounter(address-1);
    }
}