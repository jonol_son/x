package interpreter.bytecodes;

import interpreter.VirtualMachine;

 /**
  * LabelCode.java 
  * @author Jonathan
  */

public class LabelCode extends ByteCode
{
    private String label;
    
    public LabelCode(){}

    //init constructor
    public void init(String args) 
    {
        label = args;
    }

    //returns args
    public String getArgs() 
    {
        return label;
    }
    
    //execute method
    public void execute(VirtualMachine vm) {}

}