package interpreter.bytecodes;

import interpreter.VirtualMachine;

 /**
  * LitCode.java 
  * @author Jonathan
  */

public class LitCode extends ByteCode 
{
    private int value;
    private String arg;
    public String id; 
            
    public LitCode(){}

    public void init(String args) 
    {
        arg = args;
        String num = args.split(" ")[0];
        value = Integer.parseInt(num);
        String argValues[] = args.split(" ");
        if (argValues.length > 1)
            id = argValues[1];
        else
            id = "";
    }

    //returns args
    public String getArgs() 
    {
        return arg;
    }
    
    //execute method
    public void execute(VirtualMachine vm) 
    {
        vm.pushRuntimeStack(value);
    }
}