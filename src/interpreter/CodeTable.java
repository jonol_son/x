package interpreter;

import java.util.HashMap;

/**
 * CodeTable.java
 * initializes the code hashmap and returns the code for a given code
 * @author Jonathan
 */

public class CodeTable 
{
    private static HashMap<String, String> codes;

    /**
     * initializer of the code hashmap
     */
    public static void init() 
    {
        codes = new HashMap<String, String>() 
        {
            {
                put("LIT", "LitCode");
                put("HALT", "HaltCode");
                put("POP", "PopCode");     
                put("FALSEBRANCH","FalseBranchCode");
                put("GOTO","GotoCode");
                put("STORE","StoreCode");
                put("LOAD","LoadCode");
                put("ARGS","ArgsCode");
                put("CALL","CallCode");
                put("RETURN","ReturnCode");
                put("BOP","BopCode");
                put("READ","ReadCode");
                put("WRITE","WriteCode");
                put("LABEL","LabelCode");
                put("DUMP","DumpCode");
                put("FORMAL","FormalCode");
                put("FUNCTION","FunctionCode");
                put("LINE","LineCode");
            };
        };
    }

    /**
     * Returns the code corresponding to a given code
     * @param code
     * @return 
     */
    public static String get(String code) 
    {
        return codes.get(code);
    }
}