package interpreter;

import interpreter.debugger.DebugByteCodeLoader;
import interpreter.debugger.DebugVM;
import interpreter.debugger.DebugUI;
import interpreter.debugger.SourceCodeLoader;
import interpreter.debugger.SourceLine;
import java.io.IOException;
import java.util.ArrayList;

 /**
  * Interpreter.java
  * The interpreter runs the interpreter and 1) performs all initializations
  * 2) loads the byte codes from the file and 3) runs the virtual machine
  * @author Jonathan
  */
public class Interpreter {

	ByteCodeLoader bcl;
        int debugMode = 0;
    private ArrayList<SourceLine> sourceCode;
        /**
         * initializes a new bytecode loader object with file name
         * @param codeFile 
         */

    /**
     * initializes a new bytecode loader object with file name
     * @param debugFlag
     * @param codeFile
     */
    public Interpreter(int debugFlag, String codeFile) 
        {
            try
            {
                if(debugFlag==1)
                {
                    debugMode = 1;
                    CodeTable.init();
                    bcl = new DebugByteCodeLoader(codeFile);
                    sourceCode = SourceCodeLoader.load(codeFile);
                }
                else
                {
                        CodeTable.init();
                        bcl = new ByteCodeLoader(codeFile);
                }
            } catch (IOException e) 
            {
                    System.out.println("**** " + e);
            }
	}

        /**
         * runs the virtual machine
         * @throws IOException
         * @throws ClassNotFoundException
         * @throws InstantiationException
         * @throws IllegalAccessException 
         */
	void run() throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException 
        {
            if(debugMode==1)
            {
                Program program = bcl.loadCodes();
		VirtualMachine vm = new DebugVM(program, sourceCode);
                DebugUI.displayUI((DebugVM)vm);
                 
            }
            else
            {
                Program program = bcl.loadCodes();
		VirtualMachine vm = new VirtualMachine(program);
		 vm.executeProgram();
            }
	}

        /**
         * Creates interpreter object and initializes the codes of the file
         * @param args
         * @throws IOException
         * @throws ClassNotFoundException
         * @throws InstantiationException
         * @throws IllegalAccessException 
         */
	public static void main(String args[]) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException 
        {
		if (args.length == 0) 
                {
			System.out.println("***Incorrect usage, try: java interpreter.Interpreter <file>");
			System.exit(1);
		}
                Interpreter interpreter;
                        
                if (args[0].equals("-d"))
                {
                    interpreter = new Interpreter(1, args[1]);
                }
                else
                {
                    interpreter = new Interpreter(0, args[0]);
                }
                interpreter.run();
	}
}