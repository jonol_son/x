package interpreter.debugger;

import interpreter.ByteCodeLoader;
import interpreter.CodeTable;
import java.io.IOException;

/**
 * Bytecodeloader for the debugger
 * @author Jonathan
 */

public class DebugByteCodeLoader extends ByteCodeLoader 
{
    /**
     * loads the programpath into the bytecodeloader 
     * @param programPath
     * @throws IOException 
     */
    public DebugByteCodeLoader(String programPath) throws IOException 
    {
        //add correct prefix
        super(programPath + ".x.cod");
    }
    /**
     * returns the codeclass
     * @param code
     * @return codeclass
     */
    public String getCodeClass(String code) 
    {
        //if the code is a debuggercode
        if (code.matches("LINE|LIT|FORMAL|FUNCTION|POP|RETURN"))
            return "debuggerByteCodes." + CodeTable.get(code);
        else
            return CodeTable.get(code);
    }
}