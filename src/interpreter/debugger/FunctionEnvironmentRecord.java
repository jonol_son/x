package interpreter.debugger;

import java.util.Iterator;
import java.util.Set;

/**
 * FuntionEnvirnmentalRecord maintains the symbol table and is responsible for 
 * 
 * @author Jonathan
 */

public class FunctionEnvironmentRecord 
{
    private Table table;
    private int currentLine, beginLine, endLine;
    private String functionName = "-";

    /**
     * FunctionEnvironmentRecord unit test 
     * @param args 
     */
    /*public static void main(String args[])
    {
        FunctionEnvironmentRecord fctEnvRecord = new FunctionEnvironmentRecord();
        
        //BS
        fctEnvRecord.beginScope();
        System.out.print(fctEnvRecord.dump());
        
        //Function g 1 20
        fctEnvRecord.functionSet("g",1,20);
        System.out.print(fctEnvRecord.dump());
        
        //Line 5
        fctEnvRecord.setCurrentLine(5);
        System.out.print(fctEnvRecord.dump());
        
        //Enter a 4
        fctEnvRecord.enter("a",4);
        System.out.print(fctEnvRecord.dump());
        
        //Enter b 2	
        fctEnvRecord.enter("b",2);
        System.out.print(fctEnvRecord.dump());
        
        //Enter c 7	
        fctEnvRecord.enter("c",7);
        System.out.print(fctEnvRecord.dump());
        
        //Enter a 1	
        fctEnvRecord.enter("a",1);   
        System.out.print(fctEnvRecord.dump());
        
        //Pop 2	
        fctEnvRecord.pop(2);
        System.out.print(fctEnvRecord.dump());
        
        //Pop 1		
        fctEnvRecord.pop(1);
        System.out.print(fctEnvRecord.dump());
    }*/
    
    /**
     * initializer
     */
    public FunctionEnvironmentRecord() 
    {
        table = new Table();
        table.beginScope();
    }
    

    /**
     * helper method to set function
     * @param name
     * @param begin
     * @param end 
     */
    public void functionSet(String name, int begin, int end)
    {
        this.setFunctionName(name);
        this.setBeginLine(begin);
        this.setEndLine(end);        
    }
    
    /**
     * sets the begin line number
     * @param line 
     */
    public void setBeginLine(int line) 
    {
        beginLine = line;
    }

    /**
     * sets the end line number
     * @param line 
     */
    public void setEndLine(int line) 
    {
        endLine = line;
    }

    /**
     * sets the current function name
     * @param name 
     */
    public void setFunctionName(String name) 
    {
        this.functionName = name;
    }
    
    /**
     * enters variable into table with offset value
     * @param id
     * @param offset 
     */
    public void enter(String variable, int offset) 
    {
        table.put(variable, offset);
    }

    /**
     * sets the currentline
     * @param line 
     */
    public void setCurrentLine(int line) 
    {
        currentLine = line;
    }

    /**
     * pops variables off the table
     * @param popCount 
     */
    public void pop(int popCount) 
    {
        table.popValues(popCount);
    }

    /**
     * gets the function Name
     * @return functionName
     */
    public String getFunctionName() 
    {
        return functionName;
    }

    /**
     * get the begin line
     * @return beginline
     */
    public int getBeginLine() 
    {
        return beginLine;
    }

    /**
     * get the end line
     * @return endline
     */
    public int getEndLine() 
    {
        return endLine;
    }

    /**
     * get the current line
     * @return currentline
     */
    public int getCurrentLine() 
    {
        return currentLine;
    }

    /**
     * returns the set of all scoped variables
     * @return set of variables
     */
    public Set<String> getVariables() 
    {
        return table.keys();
    }

/**
 * gets the offset value for the variable 
 * @param variable
 * @return offset
 */    
    public Object getVariableOffset(String variable) 
    {
        return table.get(variable);
    }
    
    /**
     * builds a dump string from the function environment record
     * @return currentrecord dump
     */
    public String dump()
    {
        String builder = "(<";
        Set<String> keys = this.getVariables();
        Iterator<String> it = keys.iterator();
               
        if (it.hasNext())
        {
            String curr = it.next();
            while(it.hasNext())
            {
                builder += curr + "/"+ this.getVariableOffset(curr)+",";
                curr = it.next();
            }
            builder += curr + "/"+ this.getVariableOffset(curr);
        }
        builder += ">, " + this.getFunctionName() + ", "+ this.getBeginLine() +", "+ this.getEndLine()+", " + this.getCurrentLine() +")" + "\n";
        return builder;
    }
}