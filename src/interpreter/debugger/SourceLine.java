package interpreter.debugger;

/**
 * sourceline and breakpoint pairs
 * @author Jonathan
 */
public class SourceLine 
{
    private String sourceLine;
    private boolean isSet;

    /**
     * sets breakpoint for give sourceline
     * @param sourceLine
     * @param setBreakPoint 
     */
    public SourceLine(String sourceLine, boolean setBreakPoint) 
    {
        this.isSet = setBreakPoint;
        this.sourceLine = sourceLine;
    }

    /**
     * returns the sourceline
     * @return sourceline
     */
    public String getSourceLine() 
    {
        return sourceLine;
    }

    /**
     * returns whether or not a breakpoint is set
     * @return bool isset 
     */
    public boolean isBreakPointSet() 
    {
        return isSet;
    }

    /**
     * sets a breakpoint
     * @param breakPoint 
     */
    public void setBreakPoint(boolean breakPoint) 
    {
        isSet = breakPoint;
    }
}