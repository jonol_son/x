package interpreter.debugger;

import interpreter.Program;
import interpreter.RunTimeStack;
import interpreter.VirtualMachine;
import interpreter.bytecodes.ByteCode;
import java.util.ArrayList;
import java.util.Set;
import java.util.Stack;

/**
 * debugVirtualMachine
 * @author Jonathan
 */

public class DebugVM extends VirtualMachine 
{
    private Stack<FunctionEnvironmentRecord> environmentStack;
    private ArrayList<SourceLine> sourceCode;
    private ByteCode currentByteCode;
    private int runMode, lineChangeFlag, traceFlag;
    private String trace = "";
    
/**
 * DebugVM method which sets up the environment of the debugger vm
 * @param program
 * @param sourceCode 
 */
    public DebugVM(Program program, ArrayList<SourceLine> sourceCode) 
    {
        //call super and set the vm's attributes
        super(program);
        this.isRunning = true;
        this.pc = 0;        
        this.runStack = new RunTimeStack();
        this.returnAddrs = new Stack<>();
        this.sourceCode = sourceCode;      
        currentByteCode = null;
        environmentStack = new Stack<>();
        lineChangeFlag = 0;
        traceFlag = 0;

        //setup the main method
        FunctionEnvironmentRecord main = new FunctionEnvironmentRecord();
        main.setFunctionName("main");
        main.setBeginLine(1);
        main.setEndLine(sourceCode.size());
        main.setCurrentLine(1);
        environmentStack.add(main);
    }

    /**
     * executeProgram method
     */
    public void executeProgram() 
    {
        int environmentStackSize = environmentStack.size();
        while (checkContinue(environmentStackSize) && isRunning) 
        {
            currentByteCode = program.getCode(pc);
            currentByteCode.execute(this);
            pc++;
        }
        runMode = -1;
       
        boolean intrinsic = true;
        if (environmentStack.peek().getBeginLine() > 0) 
                intrinsic = false;
        
        if(!intrinsic && traceFlag==1 && environmentStack.size() != environmentStackSize)
            System.out.print(getTrace());
    }

    /**
     * method to check isRunning
     * @return isRunning 
     */
    public boolean isRunning() 
    {
        return isRunning;
    }

    /**
     * sets the program's run mode
     * @param mode 
     */
    public void setRunMode(int mode)
    {
            runMode = mode;
    }

    /**
     * checks the continue condition
     * @param environmentStackSize
     * @return continueFlag
     */
    public boolean checkContinue(int environmentStackSize) 
    {
        boolean continueFlag = false;
        //continue
        if (runMode == 0) 
        {
            continueFlag = !isBreakPointSet(getCurrentLine()) || environmentStackSize == environmentStack.size() || lineChangeFlag == 0;
            if (isBreakPointSet(getCurrentLine()) && lineChangeFlag == 1)
                continueFlag = false;
        }
        //stepout
        else if (runMode == 1)
        {
            continueFlag = environmentStack.size() >= environmentStackSize;
            if (isBreakPointSet(getCurrentLine()) && lineChangeFlag == 1)
                continueFlag = false;
        }
        //stepover
        else if (runMode == 2)
        {
            //the current line changes with env stack size remaining the same
            if (lineChangeFlag == 0)
                continueFlag = true;
            else
                continueFlag = false;
        }
        //into
        else if (runMode == 3)
        {
            continueFlag = environmentStack.size() <= environmentStackSize;
                
            // If stepping into a non-intrinsic function, allow for the FormalCode to be read in
            if (!continueFlag && environmentStack.peek().getBeginLine() > 0 && currentByteCode.getId().matches("FUNCTION"))
                    continueFlag = true;
        }
        if (lineChangeFlag == 1)
            lineChangeFlag = 0;

        return continueFlag;
    }

    /**
     * gets current line
     * @return currline
     */
    public int getCurrentLine() 
    {
         return  environmentStack.peek().getCurrentLine();
    }

    /**
     * sets current line
     * @param lineNumber 
     */
    public void setCurrentLine(int lineNumber) 
    {
        if(lineNumber >= 0 && environmentStack.size() == runStack.frameSize() + 1) 
        {
            FunctionEnvironmentRecord record = environmentStack.pop();
            record.setCurrentLine(lineNumber);
            environmentStack.add(record);
            lineChangeFlag = 1;
        }
    }

    /**
     * gets the contents of sourcecode at line number
     * @param lineNumber
     * @return linecontents
     */
    public String getSourceLine(int lineNumber) 
    {
        String linecontents = sourceCode.get(lineNumber - 1).getSourceLine();
        return linecontents;
    }

    /**
     * returns the size of sourcecode
     * @return sourceCodesize
     */
    public int getSourceSize() 
    {
        return sourceCode.size();
    }

    /**
     * checks if breakpoint is set
     * @param line
     * @return isSet
     */
    public boolean isBreakPointSet(int line) 
    {
        boolean isSet;
        if (line > 0)
            isSet = sourceCode.get(line - 1).isBreakPointSet();
        else
            isSet = false;
        return isSet;
    }
    
    /**
     * sets a breakpoint at specified line number if valid
     * @param lineNumber
     * @param breakPoint
     * @return breakpointset
     */
    
    public boolean setBreakPoint(int lineNumber, boolean breakPoint) 
    {
        boolean set;
        SourceLine sourceLine = sourceCode.get(lineNumber);
        String line = sourceLine.getSourceLine();
        if (isValidBreakPoint(line)) 
        {         
            sourceLine.setBreakPoint(breakPoint);
            set = true;
        } 
        else 
        {
            set = false;
        }
        return set;
    }

    /**
     * returns true or false if a breakpoint can be made on a line
     * @param line
     * @return boolean isValid
     */
    public boolean isValidBreakPoint(String line) 
    {
        boolean isValid;
        if (line.contains("=")||line.contains("{")||line.contains("return")||line.contains("int")||line.contains("boolean")||line.contains("if")||line.contains("while"))
            isValid = true;
        else 
            isValid = false;
        return isValid;
    }

    /**
     * adds function record
     * @param name
     * @param beginLine
     * @param endLine 
     */
    public void addFunctionRecord(String name, int beginLine, int endLine) 
    {
        FunctionEnvironmentRecord record = new FunctionEnvironmentRecord();
        record.functionSet(name, beginLine, endLine);
        record.setCurrentLine(getCurrentLine());
        environmentStack.add(record);
        
        if (traceFlag==1 && environmentStack.peek().getBeginLine() > 0)
            logTrace(0);
    }

    /**
     * adds environmental record entry
     * @param id
     * @param offset 
     */
    public void addRecordEntry(String id, int offset) 
    {
        FunctionEnvironmentRecord record = environmentStack.pop();
        record.enter(id, offset);
        environmentStack.add(record);
    }

    /**
     * pop helper
     * @param numberOfPops 
     */
    public void popRecordEntries(int numberOfPops) 
    {
        FunctionEnvironmentRecord record = environmentStack.pop();
        record.pop(numberOfPops);
        environmentStack.add(record);
    }
    
    /**
     * pops function method
     */
    public void popFunctionRecord() 
    {
        if (traceFlag == 1 && environmentStack.peek().getBeginLine() > 0)
            logTrace(1);
        environmentStack.pop();
    }
    
    /**
     * 
     * @param functionType 
     */
    private void logTrace(int functionType) 
    {
        String functionName;
        functionName = environmentStack.peek().getFunctionName().split("<<")[0];
        //indenting
        for (int i = 0; i < environmentStack.size(); i++)
            trace = trace + " ";

        if (functionType == 0) 
        {
            String funcArgs = "";
            for (int index = runStack.peekFrame(); index < runStack.runtimeStackSize(); index++) 
            {
                funcArgs += runStack.get(index);
                if (index != runStack.runtimeStackSize()-1)
                    funcArgs += ",";
            }
            trace += functionName + "(" + funcArgs + ")" + "\n";
        } 
        else 
        {
            int returnValue = runStack.peek();
            trace += "exit: " + functionName + ": " + returnValue + "\n";
        }
    }
    /**
     * gets beginning line number
     * @return beginlinenumber
     */
    public int getBeginFunctionLine() 
    {
        return environmentStack.peek().getBeginLine();
    }

    /**
     * gets the function last line number
     * @return lastlinenumeber
     */
    public int getLastFunctionLine() 
    {
        return environmentStack.peek().getEndLine();
    }

    /**
     * gets the function name
     * @return functionName
     */
    public String getCurrentFunctionName() 
    {
        return environmentStack.peek().getFunctionName().split("<<")[0];
    }

    /**
     * retrieves environment variables
     * @return set of variables on environment stack
     */
    public Set<String> getFunctionVariables() 
    {
        return environmentStack.peek().getVariables();
    }

    /**
     * function to get variable at offset
     * @param variable
     * @return variable@offset
     */
    public int getVariable(String variable) 
    {
        int offset = (int)environmentStack.peek().getVariableOffset(variable);
        return runStack.get(offset);
    }

    /**
     * gets the trace string
     */
    public String getTrace() 
    {
        return trace;
    }
    
    /**
     * togglestrace
     * @return traceflag
     */
    public int toggleTrace()
    {
        if (traceFlag == 1)
            traceFlag = 0;
        else
            traceFlag = 1;
        return traceFlag;
    }

    /**
     * gets the current call stack
     * @return callstack
     */
    String getCallStack() 
    {
        String callStack = "";
        int topEnvironmentStack = environmentStack.size() - 1;
        for (int i = topEnvironmentStack; i > 0; i--) 
        {
            FunctionEnvironmentRecord function = environmentStack.get(i);
            String functionName = function.getFunctionName().split("<<")[0];
            callStack = callStack + functionName + ": " + function.getCurrentLine() + "\n";
        }
        return callStack;
    }
}