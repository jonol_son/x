package interpreter.debugger;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * SourceCodeLoader for the debugger
 * @author Jonathan
 */
public class SourceCodeLoader 
{
    private SourceCodeLoader() {}

    /**
     * returns an arraylist of the sourcecode
     * @param codeFile
     * @return Array of input
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public static ArrayList<SourceLine> load(String codeFile) throws FileNotFoundException, IOException 
    {
        codeFile += ".x";
        //wrapping the reader
        BufferedReader programFile = new BufferedReader(new FileReader(codeFile));
        ArrayList<SourceLine> sourceCode = new ArrayList();
        while (programFile.ready())
        {
            sourceCode.add( new SourceLine(programFile.readLine(), false) );
        }
        return sourceCode;
    }

}