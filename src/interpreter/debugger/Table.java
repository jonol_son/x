package interpreter.debugger;

import java.util.HashMap;
import java.util.Set;

/**
 * Binder struct
 * @author Jonathan
 */
class Binder
{
    private Object value;
    private String prevtop;   
    private Binder tail;      

    Binder(Object v, String p, Binder t) 
    {
          value = v; 
          prevtop = p; 
          tail = t;
    }

    Object getValue() 
    { 
        return value; 
    }
    String getPrevtop() 
    { 
        return prevtop; 
    }
    Binder getTail() 
    { 
        return tail; 
    }
}

/**
 * Table table class responsible for keeping track of the symbol table
 * @author Jonathan
 */
public class Table 
{
  private HashMap<String,Binder> symbols;
  private String top;
  //private Binder marks;
  
  /**
   * main for unit testing
   */
  /*public static void main (String args[])
  {
      Table t = new Table();
      t.beginScope();
      t.put("s",1);
      t.put("s1",1);
      t.put("s2",1); 
  }*/

  public Table(){}

/**
 * gets the value from the symbol table with corresponding key
 * @param key
 * @return 
 */
  public int get(String key) 
  {
	int e = (Integer)symbols.get(key).getValue();
	return e;
  }

  /**
   * puts the variable and offset in the symbol table moving old value to the tail
   * @param key
   * @param value 
   */
  public void put(String variable, int value) 
  {
      Binder tail = symbols.get(variable);
      symbols.put(variable, new Binder(value, top, tail));
      top = variable;
  }

  /**
   * pops variables off the symbol table and resolves out of scope issues
   * @param popCount 
   */
  public void popValues(int popCount) 
  {
	for (int i = 0; i < popCount; i++) 
        {
            Binder temp = symbols.get(top);
            if (temp.getTail()!=null)
               symbols.put(top,temp.getTail());
	   else
               symbols.remove(top);
	   top = temp.getPrevtop();
        }
  }

  /**
   * gets the key set of variables
   * @return keyset
   */
  public Set<String> keys() 
  {
      return symbols.keySet();
  }

  /**
   * begins scope
   */
    public void beginScope() 
    {
        symbols = new HashMap<String,Binder>();
        top=null;    
    }
    /**
     * end scope
     */
    /*public void endScope()
    {
        while (top!=null)
        {
            Binder e = symbols.get(top);
            if(e.getTail()!=null)
                symbols.put(top,e.getTail());
            else
                symbols.remove(top);
            top = marks.getPrevtop();
        }
        top = marks.getPrevtop();
        marks = marks.getTail();
    }*/
}