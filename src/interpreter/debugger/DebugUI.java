package interpreter.debugger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * DebugUI
 * @author Jonathan
 */

public class DebugUI 
{
    private static DebugVM dvm;

    /**
     * prints the virtual machines call stack
     */
    private static void printCallStack() 
    {
        System.out.print(dvm.getCallStack());
    }

    private DebugUI() 
    {}

    /**
     * User command interface
     * @param virtuialMachine 
     */
    public static void displayUI(DebugVM virtuialMachine) 
    {
        dvm = virtuialMachine;
        String command = null;
        System.out.print("\t**** debug mode ****\n");
        displayFunction();
        System.out.println("Type '?' for help");
        while (dvm.isRunning()) 
        {
            try 
            {
                System.out.print("> ");
                BufferedReader input = new BufferedReader( new InputStreamReader( System.in ) );
                command = input.readLine();
                input(command.toLowerCase());
            } 
            catch (IOException ex) {}
        }
    }

    /**
     * processes user input
     * @param command 
     */
    private static void input(String command) 
    {
        String arg = "";
        if (command.split(" ").length > 1)
            arg = command.split(" ",2)[1];

        command = command.split(" ")[0];

        if (command.contains("help")||command.contains("?"))
            displayHelp();
        else if (command.contains("setb"))
            setBreakPoint(arg);
        else if (command.contains("removeb"))
            removeBreakPoint(arg);
        else if (command.contains("clearb"))
            clearBreakPoint(); 
        else if (command.contains("listb"))
            listBreakPoints();
        else if (command.contains("dispf"))
            displayFunction();
        else if (command.contains("dispv"))
            displayVariables();
        else if (command.contains("con"))
            run(0);
        else if (command.contains("out"))
            run(1);
        else if (command.contains("over"))
            run(2);
        else if (command.contains("in"))
            run(3);
        else if (command.contains("trace"))
            toggleTrace();
        else if (command.contains("call"))
            printCallStack();
        else if (command.contains("q")||command.contains("halt"))
            quit();
        else
            System.out.println("Invalid command, type '?' for help.");
    }
    
    /**
     * displays help
     * @return help
     */
    private static String displayHelp() 
    {
        String help = "\t\t-HELP MENU-\n\n";
        help += "Command \tDescription \n\n";
        help += "?/help \t\tDisplays this menu\n";
        help += "setb \t\tsets breakpoint(s) (format setb #)\n";
        help += "removeb \tremoves specific breakpoint(s) (format removeb # or removeb # #)\n";
        help += "clearb \t\tclears ALL breakpoints \n";
        help += "listb \t\tdisplays current breakpoint(s) \n";
        help += "dispf \t\tdisplays the current function\n";
        help += "dispv \t\tdisplays variables\n";
        help += "continue \tcontinues with program execution\n";
        help += "sout/out \tsteps out\n";
        help += "sin/in \t\tsteps into\n";
        help += "sover/over \tsteps over\n";
        help += "trace \t\ttogggles function trace\n";
        help += "printcall/call \tprints callstack\n";
        help += "halt/quit \tquits the current program\n";
        
        System.out.println(help);
        return help;
    }

    /**
     * Sets breakpoints
     * @param lineNumbers 
     */
    private static void setBreakPoint(String lineNumbers) 
    {
        if(lineNumbers.matches("[\\d+\\s*]+"))
        {
            String breakpointset = "";
            for (String line : lineNumbers.split(" ")) 
            {
                int lineNumber = Integer.parseInt(line);

                if (lineNumber <= dvm.getSourceSize()) 
                {
                    if (dvm.setBreakPoint(lineNumber - 1, true))
                        breakpointset += lineNumber + " ";
                    else
                        System.out.println("Cannot set breakpoint at line " + lineNumber);
                } 
                else
                    System.out.println(lineNumber + " is not a valid line number");
            }

            if (!breakpointset.isEmpty())
                System.out.println("BreakPoints set: " + breakpointset);
        }
        else
        {
            System.out.println("Please choose a valid line number");
        }
    }

    /**
     * clears all breakpoints 
     */
    private static void clearBreakPoint() 
    {
        String breakpointsremoved = "";
        for (int lineNumber = 1; lineNumber<dvm.getSourceSize(); lineNumber++) 
        {
                dvm.setBreakPoint(lineNumber - 1, false);
        }

        if (!breakpointsremoved.isEmpty())
            System.out.println("All break points have been removed");
        
        System.out.println("Cleared all breakpoints!");
    }
     /**
     * remove breakpoint
     * @param lineNumbers 
     */
    private static void removeBreakPoint(String lineNumbers) 
    {
        String breakpointsremoved = "";
        for (String line : lineNumbers.split(" ")) 
        {
            int lineNumber = Integer.parseInt(line);

            if (lineNumber <= dvm.getSourceSize()) 
            {
                if (dvm.setBreakPoint(lineNumber - 1, false))
                    breakpointsremoved += lineNumber + " ";
            } 
            else
                System.out.println(lineNumber + " is not a valid line number");
        }

        if (!breakpointsremoved.isEmpty())
            System.out.println("BreakPoints removed: " + breakpointsremoved);
    }

    /**
     * list break point method
     */
    private static void listBreakPoints() 
    {
        String breakpoints = "";
        for (int line = 1; line <= dvm.getSourceSize(); line++)
            if (dvm.isBreakPointSet(line))
                breakpoints += line + " ";

        if (!breakpoints.isEmpty())
            System.out.println("Current BreakPoints: " + breakpoints);
        else
            System.out.println("There are no breakpoints currently set");
    }

    /**
     * displays function
     * @return functionString
     */
    private static String displayFunction() 
    {
        int start = dvm.getBeginFunctionLine();
        int end = dvm.getLastFunctionLine();
        int current = dvm.getCurrentLine();

        //
        if(0>dvm.getBeginFunctionLine())
        {
            System.out.print(dvm.getCurrentFunctionName() + " Function\n");
            return dvm.getCurrentFunctionName();
        }    
        else
        {
            String lineBuilder = "";
            for (int line = start; line <= end; line++) 
            {
                if (dvm.isBreakPointSet(line))
                    lineBuilder += "*";
                else
                    lineBuilder += " ";

                if(line<10)
                    lineBuilder += line + ". " + dvm.getSourceLine(line);
                else
                    lineBuilder += line + "." + dvm.getSourceLine(line);

                if (line == current)
                    lineBuilder += " <-----";

                lineBuilder += "\n";
            }
            System.out.println(lineBuilder);
            return lineBuilder;
        }
    }
    
   /**
    * displays variables
    * @return variables
    */
    
    private static String displayVariables() 
    {
        String variables = "";
        for (String variable : dvm.getFunctionVariables()) 
        {
            variables = variables + variable + ": " + dvm.getVariable(variable) + "\n";
        }
        System.out.println(variables);
        return variables;
    }

    /**
     * method to halt the dvm
     */
    private static void quit() 
    {
        System.out.println("******Execution Halted*******");
        dvm.halt();
    }   
    
    /**
     * runs the program to execute in certain mode 
     * @param runMode
     * @return runmode
     */
    private static int run(int runMode)
    {
        //continue
        dvm.setRunMode(runMode);
        
        dvm.executeProgram();
        displayFunction();
        return runMode;
    }
    /**
     * toggles the dvm function trace
     */
    public static void toggleTrace()
    {
        int trace = dvm.toggleTrace();
        if (trace == 1)
            System.out.println("Function Trace is on");
        else
            System.out.println("Function Trace is off");
    }
}